const Telegram = require('telegraf/telegram')
const {BOT_TOKEN, WORKER_LOOP} = require('./config')

const telegram = new Telegram(BOT_TOKEN)

const trackerService = require('./services/tracker')

const sendTelegramMessageToUser = (userId, txt) => telegram.sendMessage(userId, txt)

const sendUsersTradingAlertMessage = ({symbol, goalPrice, userId}) => sendTelegramMessageToUser(userId, `Exchange ${symbol} has reached your goal ${goalPrice}\nThis alert will be removed`)


const sendUserAlert = ({baseAsset, quoteAsset, userId, goalPrice, _id}) => {
    sendUsersTradingAlertMessage({userId, symbol: `${baseAsset}/${quoteAsset}`, goalPrice})
    trackerService.removeTradingAlert(_id)
}
const getTradingPairsAndCheckOnAlert = async () => {
    const availableTradingPairs = await trackerService.getTradingAlerts()
    for await (const {baseAsset, quoteAsset, userId, goalPrice, _id} of availableTradingPairs) {
        const latestTrades = await trackerService.getTrades({symbol: `${baseAsset}${quoteAsset}`})
        latestTrades.length = 1
        console.log('FOR DEV')
        latestTrades.forEach(c => c.price >= goalPrice && sendUserAlert({
            baseAsset,
            quoteAsset,
            userId,
            goalPrice,
            _id
        }))
    }
}

const setTimeOutForLaunch = startDate =>  setTimeout(launch, WORKER_LOOP - ((new Date - startDate)))

const launch = () => {
    const startDate = new Date();
    getTradingPairsAndCheckOnAlert()
        .then((result) => {
            setTimeOutForLaunch(startDate)
        })
        .catch(err => {
            if (err)
                console.log('error', err);
            setTimeOutForLaunch(startDate)
        });
}

module.exports = {
    launch
}
