const {connect} = require("./modules/mongodb");

const bot = require('./bot')
const job = require('./job')

const start = async () => {
    await connect()
    bot.launch()
    job.launch()
}

start().catch(console.log)
