const TrackerRepository = require('../repositories/tracker')
const BinanceAPi = require('../modules/request')
const {CACHE_LIVE_SEC} = require('../config')

const createTradingPairsAndTextObject = (pairs) => pairs.symbols.filter(c => c.status === "TRADING")
    .reduce((acc, {baseAsset, quoteAsset}) => {
        acc.text += `baseAsset - ${baseAsset} \nquoteAsset - ${quoteAsset}\nsymbol - ${baseAsset}/${quoteAsset}\n\n`

        if (!acc.pairs.hasOwnProperty(baseAsset)) acc.pairs[baseAsset] = [quoteAsset]
        else acc.pairs[baseAsset].push(quoteAsset)

        return acc
    }, {text: 'Trading pairs \n\n', pairs: {}})

class TrackerService {
    #repository
    #api
    #tradingPairs = {}
    #tradingPairsText
    #tradingPairsLastUpdateDateInMils

    constructor() {
        this.#repository = TrackerRepository;
        this.#api = BinanceAPi
        BinanceAPi.getAllTradingPairs()
            .then(tradingPairs => {
               const {text, pairs} = createTradingPairsAndTextObject(tradingPairs)
                this.#tradingPairs = pairs
                this.#tradingPairsText = text
                this.#tradingPairsLastUpdateDateInMils = new Date().getDate()
            })
            .catch((e) =>{
                console.log(e.message)
                process.exit(-1)
            } )
    }

    cacheTradingPairResults(tradingPairs, text) {
        this.#tradingPairs = tradingPairs
        this.#tradingPairsText = text
        this.#tradingPairsLastUpdateDateInMils = new Date().getDate()
    }

    getTradingPairsCache() {
        return new Date().getTime() - this.#tradingPairsLastUpdateDateInMils > CACHE_LIVE_SEC * 1000 ? this.#tradingPairsText : null
    }

    async getAllTradingPairs() {
        const tradingPairs = await BinanceAPi.getAllTradingPairs().catch(this.#api.errorHandler)
        let result = this.getTradingPairsCache();
        if (!result) {
            const {text, pairs} = createTradingPairsAndTextObject(tradingPairs)
            result = text
            this.cacheTradingPairResults(pairs, result)
        }
        return result
    }

    async addTradeAlert({userId, baseAsset, quoteAsset, goalPrice}) {
        if (!this.#tradingPairs.hasOwnProperty(baseAsset) || this.#tradingPairs[baseAsset].includes(quoteAsset)) {
            const err = new Error()
            err.responseText = 'Pairs doesnt exists'
            throw err
        }

        const {nModified} = await this.#repository.addTrade({userId, baseAsset, quoteAsset, goalPrice}).catch(this.#api.errorHandler)

        return nModified > 0
    }

    getTradingAlerts(limit = 1000) {
        return this.#repository.getTrades(limit)
    }

    getTrades({symbol, limit}) {
        return this.#api.getTrades({symbol, limit})
    }

    removeTradingAlert(_id) {
        return this.#repository.deleteTrade(_id)
    }
}


module.exports = new TrackerService()
