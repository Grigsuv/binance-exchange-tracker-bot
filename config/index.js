require('dotenv').config()

module.exports = {
    BOT_TOKEN: process.env.BOT_TOKEN,
    DB_URL: process.env.DB_URL,
    BINANCE_API: process.env.BINANCE_API,
    WORKER_LOOP: process.env.WORKER_LOOP,
    CACHE_LIVE_SEC: process.env.CACHE_LIVE_SEC,
    RATE_LIMITER_COUNT: process.env.RATE_LIMITER_COUNT,
    RATE_LIMITER_WINDOW: process.env.RATE_LIMITER_WINDOW,
}
