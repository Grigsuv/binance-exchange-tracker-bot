const {tradesModel} = require('../models')

class TrackerRepository {
    #model

    constructor() {
        this.#model = tradesModel
    }

    getTrades(limit) {
        return this.#model.find({}, {}, {limit}).lean().exec()
    }

    addTrade({userId, baseAsset, quoteAsset, goalPrice}) {
        return this.#model.updateOne({$and: [{userId}, {baseAsset}, {quoteAsset}]}, {
            userId,
            baseAsset,
            quoteAsset,
            goalPrice
        }, {upsert: true})
    }

    deleteTrade(_id){
        return this.#model.findOneAndDelete({_id}).exec()
    }
}


module.exports = new TrackerRepository()
