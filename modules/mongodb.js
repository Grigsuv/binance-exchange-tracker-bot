const mongoose = require('mongoose');
const {DB_URL} = require('./../config');


module.exports.connect = () => mongoose.connect(DB_URL, {
    useCreateIndex: true,
    useNewUrlParser: true,
    useUnifiedTopology: true
});
