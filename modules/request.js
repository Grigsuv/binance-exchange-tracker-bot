const axios = require('axios')
const {BINANCE_API} = require('../config')

class BinanceAPIModule {
    #baseApi
    #requestInstance

    constructor() {
        this.#baseApi = BINANCE_API;
        this.#requestInstance = axios
    }

    async makeRequestWithDataResponse({method, data = {}, params = {}, url}) {
        const {data: responseData} = await this.#requestInstance({
            method,
            params,
            data,
            url: `${this.#baseApi}/${url}`
        })
        return responseData
    }

    async getAllTradingPairs() {
        return this.makeRequestWithDataResponse({method: "GET", url: `exchangeInfo`})
    }

    async getTrades({symbol, limit = 10}) {
        return this.makeRequestWithDataResponse({method: "GET", url: `trades`, params : {symbol, limit}})
    }

    errorHandler (err) {
        console.log(err.message)
        throw new Error()
    }
}
module.exports = new BinanceAPIModule()
