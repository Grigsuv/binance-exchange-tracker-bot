const {Schema} = require('mongoose');

const Trades = new Schema({
    userId: {
        type: Number,
        required: true
    },
    baseAsset:{
        type: String,
        required: true
    },
    quoteAsset:{
        type: String,
        required: true
    },
    goalPrice:{
        type: Number,
        required: true
    },
    updated: {
        type: Date,
        default: Date.now,
    },
});

Trades.pre('update', function (next) {
    this.updated = Date.now;
    next();
});

module.exports = Trades;
