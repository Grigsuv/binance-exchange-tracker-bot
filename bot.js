const {Telegraf} = require('telegraf')
const {BOT_TOKEN, RATE_LIMITER_COUNT, RATE_LIMITER_WINDOW} = require('./config')
const trackerService = require('./services/tracker')
const rateLimit = require('telegraf-ratelimit')

const limitConfig = {
    window: RATE_LIMITER_WINDOW,
    limit: RATE_LIMITER_COUNT,
    onLimitExceeded: (ctx, next) => ctx.reply('Rate limit exceeded')
}

const bot = new Telegraf(BOT_TOKEN);

bot.use(rateLimit(limitConfig))

bot.catch((err, ctx) => {
    console.log(`Ooops, encountered an error for ${ctx.updateType}`, err)
    ctx.reply(err.responseText || 'O_o, try again, smt went wrong')
})

bot.command('getAllTradingPairs', async ({message: {from: sender, text: messageText}, reply}) =>
    reply(await trackerService.getAllTradingPairs())
)

bot.command('alert', async ({message: {from: sender, text: messageText}, reply}) => {
    const [, tradeSymbol, goalPrice] = messageText.split(' ');
    const [baseAsset, quoteAsset] = tradeSymbol.split('/');

    if (typeof baseAsset === "undefined" || typeof quoteAsset === "undefined" || typeof goalPrice === "undefined")
        return reply(`Wrong input,\n Command is /alert $symbol goal,\n For example /alert DNT/ETH 9000\n You can get full symbols list using command /getAllTradingPairs`)

    const updated = await trackerService.addTradeAlert({userId: sender.id, baseAsset, quoteAsset, goalPrice})
    const responseText = updated ? 'Alter has been updated' : "Alter has been added";

    reply(responseText)
})
module.exports = bot
